CREATE TABLE players
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(20) NOT NULL UNIQUE
);

CREATE TABLE distributions
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    solution INT NOT NULL
);




CREATE TABLE distributions
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    solution INT NOT NULL
);
CREATE TABLE gameplays
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    user_id INT NOT NULL,
    distribution_id INT
);
CREATE TABLE moves_history
(
    game_id INT NOT NULL,
    step INT NOT NULL,
    guess INT NOT NULL,
    PRIMARY KEY (game_id, step)
);
CREATE TABLE users
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(20) NOT NULL
);
CREATE UNIQUE INDEX unique_solution ON distributions (solution);
ALTER TABLE gameplays ADD FOREIGN KEY (distribution_id) REFERENCES distributions (id);
ALTER TABLE gameplays ADD FOREIGN KEY (user_id) REFERENCES users (id);
CREATE INDEX FK_distribution_id ON gameplays (distribution_id);
CREATE INDEX FK_gameplays_user_id ON gameplays (user_id);
ALTER TABLE moves_history ADD FOREIGN KEY (game_id) REFERENCES gameplays (id);
CREATE UNIQUE INDEX name ON players (name);
CREATE UNIQUE INDEX unique_name ON users (name);
