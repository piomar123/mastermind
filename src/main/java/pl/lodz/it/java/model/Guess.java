package pl.lodz.it.java.model;

/**
 * Class for one move in game. Allows to order chronologically using Comparable interface.
 */
public class Guess implements Comparable<Guess> {
    private int order;
    private RowState guess;

    public Guess(int order, RowState guess) {
        this.order = order;
        this.guess = guess;
    }

    public int getOrder() {
        return order;
    }

    public RowState getGuess() {
        return guess;
    }

    @Override
    public int compareTo(Guess other) {
        return (this.order - other.order);
    }
}
