package pl.lodz.it.java.model;

/**
 * Code pegs distribution: ID + RowState.
 * Immutable.
 */
public class Distribution {
    private int id;
    private RowState row;

    public Distribution(int id, RowState row) {
        this.id = id;
        this.row = row;
    }

    public int getId() {
        return id;
    }

    public RowState getRow() {
        return row;
    }
}
