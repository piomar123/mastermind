package pl.lodz.it.java.model;

/**
 * Class containing feedback with key pegs (small ones: black or white).
 */
public class Feedback {
    private final int black;
    private final int white;

    public Feedback(int black, int white){
        this.black = black;
        this.white = white;
    }

    /**
     * Gets number of black pegs - indicating correct peg & position.
     * @return numer of black pegs
     */
    public int getBlack() {
        return black;
    }

    /**
     * Gets number of white pegs - indicating misplaced peg.
     * @return number of white pegs
     */
    public int getWhite() {
        return white;
    }
}
