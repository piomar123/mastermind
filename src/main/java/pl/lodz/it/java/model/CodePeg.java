package pl.lodz.it.java.model;

/**
 * Enum with different code peg colours (big ones).
 */
public enum CodePeg {
    RED(0), GREEN(1), BLUE(2), YELLOW(3), VIOLET(4), BROWN(5);

    private int value;
    public static final int COLORS = 6;

    CodePeg(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

}
