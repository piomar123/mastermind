package pl.lodz.it.java.model;

/**
 * Class for gameplay structure - connecting Player with solution order & moves history.
 * Immutable.
 */
public class Gameplay {
    private final int id;
    private final Player player;
    private final RowState solution;
    private final RowState[] moves;


    public Gameplay(Player player, RowState solution, RowState[] moves) {
        this(-1, player, solution, moves);
    }

    public Gameplay(int id, Player player, RowState solution, RowState[] moves) {
        this.id = id;
        this.player = player;
        this.solution = solution;
        this.moves = moves;
    }

    public Gameplay(int id, Gameplay gameplay) {
        this(id, gameplay.player, gameplay.solution, gameplay.moves);
    }

    public Gameplay(Player player, RowState solution) {
        this(-1, player, solution, null);
    }

    public Player getPlayer() {
        return player;
    }

    public RowState getSolution() {
        return solution;
    }

    public RowState[] getMoves() {
        return moves;
    }

    public int getId() {
        return id;
    }
}
