package pl.lodz.it.java.model;

/**
 * One row distribution state on the game board.
 * Immutable.
 */
public class RowState {
    private int state;
    public static final int PEGS_PER_ROW = 4;

    /**
     * Creates row state basing on calculated state sum.
     * @param state state sum
     */
    public RowState(int state){
        this.state = state;
    }

    /**
     * Creates row state instance basing on array with {@link CodePeg}s.
     * @param pegs array of code pegs
     */
    public RowState(CodePeg... pegs){
        this.state = calculateState(pegs);
    }

    /**
     * Gets row state as state sum.
     * @return state sum
     */
    public int getState() {
        return state;
    }

    /**
     * Gets row state as an array of {@link CodePeg}s.
     * @return array of pegs.
     */
    public CodePeg[] getPegs(){
        int sum = this.state;
        CodePeg[] colors = CodePeg.values();
        CodePeg[] pegs = new CodePeg[PEGS_PER_ROW];
        for (int i = pegs.length - 1; i >= 0; i--) {
            pegs[i] = colors[sum % colors.length];
            sum /= colors.length;
        }
        return pegs;
    }

    private int calculateState(CodePeg[] pegs){
        int sum = 0;
        for (int p = 0; p < PEGS_PER_ROW; p++) {
            sum *= CodePeg.COLORS;
            sum += pegs[p].getValue();
        }
        return sum;
    }

}
