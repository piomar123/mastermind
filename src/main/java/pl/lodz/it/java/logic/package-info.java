/**
 * Logic layer for the Mastermind game. Allows to run with different graphical interfaces.
 */
package pl.lodz.it.java.logic;