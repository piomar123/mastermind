package pl.lodz.it.java.logic;

import pl.lodz.it.java.dao.DAOException;
import pl.lodz.it.java.dao.IDAO;
import pl.lodz.it.java.model.Feedback;
import pl.lodz.it.java.model.Gameplay;
import pl.lodz.it.java.model.Player;
import pl.lodz.it.java.model.RowState;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Main class with Mastermind logic.
 * Provides functionality for keeping game progress, checking winning state, etc.
 */
public class MastermindLogic {
    private HelpLogic helper;
    private Random random;
    private boolean gameInProgress = false;
    private int iteration = 1;
    private RowState solution;
    private boolean winner = false;
    private List<RowState> movesHistory;
    private Player currentPlayer;
    private IDAO dao;
    private Gameplay gameplay;

    public MastermindLogic(IDAO dao){
        this.dao = dao;
        helper = new HelpLogic();
        random = new Random();
    }

    /**
     * Starts a new game resetting all stats.
     */
    public void startNewGame() throws DAOException {
        gameInProgress = true;
        iteration = 1;
        winner = false;
        movesHistory = new ArrayList<>();
        solution = new RowState(random.nextInt(helper.getNumberOfPermutations()));
        gameplay = dao.addGameplay(new Gameplay(currentPlayer, solution));
    }

    /**
     * Ends current game. This method has no effect if game is not in progress.
     */
    public void endGame(){
        if(!gameInProgress) return;
        gameInProgress = false;
        gameplay = null;
    }

    /**
     * Checks given row, provides feedback and increases game counter.
     * @param guess {@link pl.lodz.it.java.model.RowState} with player's guess
     * @return {@link pl.lodz.it.java.model.Feedback} with black and white pegs
     */
    public Feedback checkRow(RowState guess){
        assert gameInProgress : "Game not in progress. Start the game first";

        Feedback feedback = helper.provideFeedback(guess, solution);
        movesHistory.add(guess);
        try {
            dao.saveMove(gameplay, guess);
        } catch (DAOException daoe){
            //
        }
        if(helper.isSolved(feedback)) {
            winner = true;
            gameInProgress = false;
        } else {
            iteration++;
        }
        // TODO 3. save history to DB (+ create table moves_history)
        return feedback;
    }


    /**
     * Tells whether game is in progress and UI should ask user when exiting game or starting new one.
     * @return true if game in progress
     */
    public boolean isGameInProgress() {
        return gameInProgress;
    }

    /**
     * Gets solved row for current game.
     * @return row with solution
     */
    public RowState getSolution() {
        return solution;
    }

    /**
     * Gets current iteration in the game - number of tries up to now.
     * @return current iteration
     */
    public int getIteration() {
        return iteration;
    }

    /**
     * Can be used to chceck if last row was correct and player guessed the solution.
     * @return true if guess was correct
     */
    public boolean isWinner() {
        return winner;
    }

    public List<RowState> getMovesHistory(){
        return movesHistory;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public void setCurrentPlayer(Player currentPlayer) {
        this.currentPlayer = currentPlayer;
    }

    public Player createPlayer(String name) throws DAOException {
        return dao.createPlayer(name);
    }

    public IDAO getDAO() {
        return dao;
    }
}
