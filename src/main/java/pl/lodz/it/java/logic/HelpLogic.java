package pl.lodz.it.java.logic;

import pl.lodz.it.java.model.CodePeg;
import pl.lodz.it.java.model.Feedback;
import pl.lodz.it.java.model.RowState;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Help methods for Mastermind logic. Provides additional functionality to the Model layer.
 */
public class HelpLogic {
    private final int numberOfPermutations;
    public HelpLogic(){
        numberOfPermutations = power(CodePeg.COLORS, RowState.PEGS_PER_ROW);
    }


    /**
     * Provides feedback pegs for given guess and solution.
     * @param guessRow row state with guessed pegs
     * @param solutionRow row state with solution
     * @return Feedback for given pegs
     */
    public Feedback provideFeedback(RowState guessRow, RowState solutionRow) {
        List<CodePeg> pegs = new ArrayList<>(Arrays.asList(guessRow.getPegs()));
        List<CodePeg> solved = new ArrayList<>(Arrays.asList(solutionRow.getPegs()));
        int white = 0, black = 0;

        for (int i = pegs.size() - 1; i >= 0; i--) {
            if (pegs.get(i) == solved.get(i)) {
                pegs.remove(i);
                solved.remove(i);
                black++;
            }
        }

        for (int p = pegs.size() - 1; p >= 0; p--) {
            for (int s = solved.size() - 1; s >= 0; s--) {
                if (pegs.get(p) == solved.get(s)) {
                    pegs.remove(p);
                    solved.remove(s);
                    white++;
                    break;
                }
            }
        }
        return new Feedback(black, white);
    }


    /**
     * Gets all number of possible code pegs permutation in one row.
     * @return permutations of code pegs
     */
    public int getNumberOfPermutations() {
        return numberOfPermutations;
    }

    public int power(int base, int exponent){
        if(exponent == 0) return 1;
        return base * power(base, exponent - 1);
    }


    /**
     * Checks if feedback has solved state.
     * @return true if guess was correct
     */
    public boolean isSolved(Feedback feedback) {
        return (feedback.getBlack() == RowState.PEGS_PER_ROW && feedback.getWhite() == 0);
    }
}
