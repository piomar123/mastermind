package pl.lodz.it.java.logic;

/**
 * Exception for logic layer.
 */
public class LogicException extends Exception {
    public LogicException(String message) {
        super(message);
    }

    public LogicException(String message, Throwable cause) {
        super(message, cause);
    }
}
