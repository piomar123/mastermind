/**
 * Data Access Objects Layer - allows communication with database or files, transparently for other layers.
 */
package pl.lodz.it.java.dao;