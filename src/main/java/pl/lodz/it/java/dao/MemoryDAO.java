package pl.lodz.it.java.dao;

import pl.lodz.it.java.model.Gameplay;
import pl.lodz.it.java.model.Player;
import pl.lodz.it.java.model.RowState;

public class MemoryDAO implements IDAO {

    @Override
    public Player[] getPlayers() throws DAOException {
        return new Player[]{new Player(1, "DemoPlayer")};
    }

    @Override
    public Player createPlayer(String name) throws DAOException {
        throw new DAOException("Not implemented");
    }

    @Override
    public void removePlayer(Player player) throws DAOException {
        throw new DAOException("Not implemented");
    }

    @Override
    public Gameplay addGameplay(Gameplay gameplay) throws DAOException {
        return gameplay;
    }

    @Override
    public Gameplay[] getGameplays(Player player) throws DAOException {
        return new Gameplay[0];
    }

    @Override
    public void saveMove(Gameplay gameplay, RowState guess) throws DAOException {

    }
}
