package pl.lodz.it.java.dao;

import pl.lodz.it.java.model.Gameplay;
import pl.lodz.it.java.model.Guess;
import pl.lodz.it.java.model.Player;
import pl.lodz.it.java.model.RowState;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;

/**
 * DAO working with MySQL JDBC connection.
 */
public class JdbcDAO implements IDAO {
    private Connection connection;

    public JdbcDAO(String server, String database, String username, String password) throws DAOException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException cnfe) {
            throw new DAOException("MySQL driver not found", cnfe);
        }

        try {
            connection = DriverManager.getConnection(
                    new StringBuilder("jdbc:mysql://").append(server).append("/").append(database).toString(),
                    username, password);
            System.out.println("Connected to database " + connection.getMetaData().getURL());
            createTables();

        } catch (SQLException sqle) {
            throw new DAOException("Error connecting to database", sqle);
        }
    }

    private void createTables() throws SQLException, DAOException {
        DatabaseMetaData metaData = connection.getMetaData();
        ResultSet resultSet = metaData.getTables(null, null, "players", null);
        if(resultSet.next()) return;

        try ( Statement stmt = connection.createStatement() ){
            stmt.execute("CREATE TABLE players (" +
                            "id INT PRIMARY KEY NOT NULL AUTO_INCREMENT," +
                            "name VARCHAR(20) NOT NULL UNIQUE" +
                          ");"
            );
            SQLWarning warning = stmt.getWarnings();
            if(warning == null) {
                System.out.println("Created table players");
            } else do {
                System.out.println("Warning: " + warning.getMessage());
                warning = warning.getNextWarning();
            } while(warning != null);
        } catch (SQLException e) {
            throw new DAOException("Cannot create the table with players", e);
        }

        try ( Statement stmt = connection.createStatement() ){
            stmt.execute("CREATE TABLE distributions (" +
                            "id INT PRIMARY KEY NOT NULL AUTO_INCREMENT," +
                            "solution INT NOT NULL UNIQUE " +
                          ");"
            );
            SQLWarning warning = stmt.getWarnings();
            if(warning == null) {
                System.out.println("Created table distributions");
            } else do {
                System.out.println("Warning: " + warning.getMessage());
                warning = warning.getNextWarning();
            } while(warning != null);
        } catch (SQLException e) {
            throw new DAOException("Cannot create the table with solutions", e);
        }
    }


    @Override
    public Player[] getPlayers() throws DAOException {
        try ( Statement stmt = connection.createStatement() ) {
            ResultSet result = stmt.executeQuery("SELECT id, name FROM players;");

            ArrayList<Player> players = new ArrayList<>();
            while(result.next()){
                int id = result.getInt("id");
                String name = result.getString("name");
                players.add(new Player(id, name));
            }
            return players.toArray( new Player[players.size()] );
        } catch (SQLException e) {
            throw new DAOException("Cannot fetch players list", e);
        }
    }

    @Override
    public Player createPlayer(String name) throws DAOException {
        try ( PreparedStatement insertStmt = connection.prepareStatement("INSERT INTO players (name) VALUES (?);", Statement.RETURN_GENERATED_KEYS) ){
            insertStmt.setString(1, name);
            insertStmt.executeUpdate();
            ResultSet result = insertStmt.getGeneratedKeys();
            result.next();
            int id = result.getInt(1);
            return new Player(id, name);

        } catch (SQLException e) {
            throw new DAOException("Cannot create the player", e);
        }
    }

    @Override
    public void removePlayer(Player player) throws DAOException {
        try ( PreparedStatement deleteStmt = connection.prepareStatement("DELETE FROM players WHERE id = ? AND name = ?;") ){
            deleteStmt.setInt(1, player.getId());
            deleteStmt.setString(2, player.getName());
            deleteStmt.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(String.format("Cannot remove the player %s [%d]", player.getName(), player.getId()), e);
        }
    }

    @Override
    public Gameplay addGameplay(Gameplay gameplay) throws DAOException {
        try ( PreparedStatement insertStmt = connection.prepareStatement("INSERT INTO gameplays (player, solution) VALUES (?, ?);", Statement.RETURN_GENERATED_KEYS) ){
            insertStmt.setInt(1, gameplay.getPlayer().getId());
            insertStmt.setInt(2, gameplay.getSolution().getState());
            ResultSet result = insertStmt.getGeneratedKeys();
            int id = result.getInt(1);
            return new Gameplay(id, gameplay);
        } catch (SQLException e) {
            throw new DAOException(String.format("Cannot save the game progress"), e);
        }
    }

    @Override
    public Gameplay[] getGameplays(Player player) throws DAOException {
        try ( PreparedStatement selectStmt = connection.prepareStatement("SELECT id, solution FROM gameplays WHERE player = ?;") ) {
            selectStmt.setInt(1, player.getId());
            ResultSet result = selectStmt.executeQuery();

            ArrayList<Gameplay> gameplays = new ArrayList<>();
            while (result.next()){
                int gameplayId = result.getInt("id");
                RowState solution = new RowState(result.getInt("solution"));
                ArrayList<Guess> moves = new ArrayList<>();
                try ( PreparedStatement movesStmt = connection.prepareStatement("SELECT move, guess FROM moves_history WHERE gameplay = ? ORDER BY move ASC;")){
                    movesStmt.setInt(1, gameplayId);
                    ResultSet movesSet = movesStmt.executeQuery();
                    while (movesSet.next()) {
                        int order = movesSet.getInt("move");
                        RowState guess = new RowState(movesSet.getInt("guess"));
                        moves.add(new Guess(order, guess));
                    }
                }
                Collections.sort(moves);
                RowState[] rowsHistory = moves.stream().map(Guess::getGuess).toArray(RowState[]::new);
                gameplays.add(new Gameplay(gameplayId, player, solution, rowsHistory));
            }
            return gameplays.toArray(new Gameplay[gameplays.size()]);
        } catch (SQLException e) {
            throw new DAOException(String.format("Cannot fetch the list of player's [%d] %s games", player.getId(), player.getName()), e);
        }
    }

    @Override
    public void saveMove(Gameplay gameplay, RowState guess) throws DAOException {

    }
}
