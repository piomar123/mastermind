package pl.lodz.it.java.dao;

import pl.lodz.it.java.model.Gameplay;
import pl.lodz.it.java.model.Player;
import pl.lodz.it.java.model.RowState;

/**
 * Interface for different implementations of DAO.
 */
public interface IDAO {
    Player[] getPlayers() throws DAOException;
    Player createPlayer(String name) throws DAOException;
    void removePlayer(Player player) throws DAOException;

    Gameplay addGameplay(Gameplay gameplay) throws DAOException;
    Gameplay[] getGameplays(Player player) throws DAOException;
    void saveMove(Gameplay gameplay, RowState guess) throws DAOException;
}
