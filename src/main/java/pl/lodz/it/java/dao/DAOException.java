package pl.lodz.it.java.dao;

/**
 * Exception for DAO layer unexpected behaviour.
 */
public class DAOException extends Exception {
    public DAOException(String message) {
        super(message);
    }

    public DAOException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Returns message containing information about this exception.
     */
    public String getMessage(){
        StringBuilder sb = new StringBuilder(super.getMessage());
        Throwable cause = super.getCause();
        if(cause != null) {
            sb.append(": ").append(cause.getMessage());
        }
        return sb.toString();
    }
}
