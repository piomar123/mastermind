package pl.lodz.it.java.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * Panel with butons under gameboard.
 */
public class ButtonsPanel extends JPanel {
    private MainWindow mainWindow;
    private JButton checkButton;

    public ButtonsPanel(MainWindow mainWindow) {
        super(new BorderLayout());
        this.mainWindow = mainWindow;
        checkButton = new JButton("Check");
        checkButton.addActionListener((ActionEvent e) -> mainWindow.checkRow());
        add(checkButton);

        setEnabled(false);
    }

    public MainWindow getMainWindow() {
        return mainWindow;
    }

    public void setEnabled(boolean value){
        super.setEnabled(value);
        checkButton.setEnabled(value);
    }
}
