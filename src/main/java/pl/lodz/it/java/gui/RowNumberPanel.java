package pl.lodz.it.java.gui;

import javax.swing.*;
import java.awt.*;

/**
 * Panel with number indicating gameboard row number (current iteration).
 */
public class RowNumberPanel extends JPanel {
    private BoardRowPanel rowPanel;
    private int iteration;
    private boolean correct = false;

    public RowNumberPanel(BoardRowPanel rowPanel, int iteration) {
        this.rowPanel = rowPanel;
        this.iteration = iteration;
    }

    @Override
    public Dimension getPreferredSize() {
        int dim = rowPanel.getHeight();
        return new Dimension(dim, dim);
    }

    @Override
    public Dimension getMaximumSize() {
        return getPreferredSize();
    }

    @Override
    public Dimension getMinimumSize() {
        return getPreferredSize();
    }

    @Override
    public void paint(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        RenderingHints rh = new RenderingHints(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_GASP);
        g2.setRenderingHints(rh);
        super.paint(g2);
        int cx = getWidth(), cy = getHeight();
        if(correct){
            g2.setColor(Color.GREEN);
        } else {
            g2.setColor(Color.LIGHT_GRAY);
        }
        g2.fillRect(0, 0, cx, cy);

        Font numFont = new Font("Arial", Font.BOLD, cy/2);
        g2.setFont(numFont);
        g2.setColor(Color.BLACK);
        FontMetrics fm = g2.getFontMetrics();
        String numStr = String.valueOf(iteration);
        int fcx = (int) fm.getStringBounds(numStr, g2).getWidth();
        int fcy = fm.getHeight();
        g2.drawString(numStr, (cx - fcx)/2, (cy - fcy)/2 + fm.getAscent());
    }

    public void setCorrect() {
        correct = true;
    }
}
