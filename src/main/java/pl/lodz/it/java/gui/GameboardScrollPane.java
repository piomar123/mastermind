package pl.lodz.it.java.gui;

import pl.lodz.it.java.model.Feedback;
import pl.lodz.it.java.model.RowState;

import javax.swing.*;
import java.awt.*;

/**
 * Top gameboard panel with pegged rows, scrollbar and generally most part of the game UI.
 */
public class GameboardScrollPane extends JScrollPane {
    private JPanel panel;
    private MainWindow mainWindow;
    private BoardRowPanel currentRowPanel;

    public GameboardScrollPane(MainWindow mainWindow) {
        this.mainWindow = mainWindow;
        setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        getVerticalScrollBar().setUnitIncrement(20);
        clearBoard();
//        BoardRowPanel boardRowPanel = new BoardRowPanel(this, 1, new RowState(51));
//        panel.add(boardRowPanel);
//        panel.add(new BoardRowPanel(this, 2, new RowState(192)));
    }


    public JPanel getPanel() {
        return panel;
    }

    public MainWindow getMainWindow() {
        return mainWindow;
    }

    /**
     * Insert interactive empty row for new guess by player.
     * @param iteration number of iteration to show
     */
    public void insertRow(int iteration) {
        insertRow(iteration, null, null, true);
    }

    /**
     * Inserts new row for pegs.
     * @param iteration number of iteration to show
     * @param rowState pegs state to show or null if empty
     * @param feedback feedback object for given pegs or null if none
     * @param interactive if true, allows player to change pegs colors
     */
    public void insertRow(int iteration, RowState rowState, Feedback feedback, boolean interactive) {
        if(currentRowPanel != null) currentRowPanel.setInteractive(false);
        currentRowPanel = new BoardRowPanel(this, iteration, rowState, feedback, interactive);
        panel.add(currentRowPanel);
        revalidate();
    }

    public RowState getCurrentRow() {
        return currentRowPanel.getRow();
    }

    public void setFeedback(Feedback feedback) {
        currentRowPanel.setFeedback(feedback);
        currentRowPanel.repaint();
    }

    /**
     * Removes all rows for new game.
     */
    public void clearBoard() {
        panel = new FillingJPanel(){
            @Override
            public Dimension getPreferredSize() {
                //int width = GameboardScrollPane.this.getWidth() - GameboardScrollPane.this.getVerticalScrollBar().getWidth() + 10;
                return new Dimension(0, (int) super.getPreferredSize().getHeight());
            }
        };
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        setViewportView(panel);
    }

    /**
     * Sets last row as correct and disables interactivity.
     */
    public void setCorrect() {
        currentRowPanel.setInteractive(false);
        currentRowPanel.setCorrect();
    }
}
