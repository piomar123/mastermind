package pl.lodz.it.java.gui;

import pl.lodz.it.java.dao.DAOException;
import pl.lodz.it.java.dao.JdbcDAO;
import pl.lodz.it.java.dao.MemoryDAO;
import pl.lodz.it.java.logic.MastermindLogic;
import pl.lodz.it.java.model.CodePeg;
import pl.lodz.it.java.model.Feedback;
import pl.lodz.it.java.model.Player;
import pl.lodz.it.java.model.RowState;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Main window of Mastermind with gameboard.
 */
public class MainWindow extends JFrame {
    private final MastermindLogic logic;
    private SelectPlayerWindow selectPlayerWindow;
    private final GUIExceptionHandler exceptionHandler;
    private GameMenuBar menuBar;
    private GameboardScrollPane gameboardScrollPane;
    private ButtonsPanel buttonsPanel;
    private Player player;

    public MainWindow(MastermindLogic logic) {
        super("Mastermind - Advanced Programming Course in Java");
        this.exceptionHandler = new GUIExceptionHandler(this);
        this.logic = logic;
        try {
            this.selectPlayerWindow = new SelectPlayerWindow(this);
        } catch (DAOException e) {
            exceptionHandler.handleException(e);
            dispose();
        }

        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        setSize(400, 700);
        setLocationRelativeTo(null);	// default system position
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                if(confirmEnding()){
                    setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
                    dispose();
                }
            }
        });

        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.BOTH;
        gbc.weightx = 1.0;
        gbc.weighty = 0.95;
        gbc.gridx = 0;
        gbc.gridy = 0;
        gameboardScrollPane = new GameboardScrollPane(this);
        add(gameboardScrollPane, gbc);

        gbc.weighty = 0.05;
        gbc.gridx = 0;
        gbc.gridy = 1;
        buttonsPanel = new ButtonsPanel(this);
        add(buttonsPanel, gbc);

        menuBar = new GameMenuBar(this);
        setJMenuBar(menuBar);

        this.selectPlayerWindow.setVisible(true);
    }

    /**
     * Allows user to confirm exiting if game in progress.
     * @return true if game can be exited, false otherwise
     */
    private boolean confirmEnding() {
        if(!logic.isGameInProgress()) return true;
        int result = JOptionPane.showConfirmDialog(this, "Are you sure to end this game?", "Game in progress", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        return result == JOptionPane.YES_OPTION;
    }

    public static void main(String[] args) throws DAOException {
        try {
            for(UIManager.LookAndFeelInfo info: UIManager.getInstalledLookAndFeels()) {
                if(info.getName().equals("Nimbus")) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
            new MainWindow(
                    new MastermindLogic(
//                        new JdbcDAO("localhost:3306", "mastermind", "mastermind", "123qwe")
                            new MemoryDAO()
                    )
            );
        } catch (Exception e) {
            GUIExceptionHandler.handleStatic(e);
        }
    }

    public MastermindLogic getLogic() {
        return logic;
    }

    /**
     * Adds closing event to the window queue.
     */
    public void pressCloseButton() {
        WindowEvent wev = new WindowEvent(this, WindowEvent.WINDOW_CLOSING);
        Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(wev);
    }

    /**
     * Starts a new game.
     */
    public void startNewGame() {
        if(confirmEnding()){
            try {
                logic.startNewGame();
                gameboardScrollPane.clearBoard();
                gameboardScrollPane.insertRow(logic.getIteration());
                buttonsPanel.setEnabled(true);
            } catch (DAOException e) {
                exceptionHandler.handleException(e);
            }
        }
    }

    /**
     * Checks created row.
     */
    public void checkRow() {
        RowState guess = gameboardScrollPane.getCurrentRow();
        if(guess == null) return;
        Feedback feedback = logic.checkRow(guess);
        gameboardScrollPane.setFeedback(feedback);
        if(logic.isWinner()) {
            gameboardScrollPane.setCorrect();
            buttonsPanel.setEnabled(false);
            JOptionPane.showMessageDialog(
                    this,
                    String.format("Congratulations! You guessed after %d tries.", logic.getIteration()),
                    "Congratulations",
                    JOptionPane.INFORMATION_MESSAGE
            );
        } else {
            gameboardScrollPane.insertRow(logic.getIteration());
        }
    }

    /**
     * Converts given code peg into {@link java.awt.Color}
     * @param peg peg to convert
     * @return color of the given peg
     */
    public Color pegToColor(CodePeg peg) {
        if(peg != null){
            switch(peg){
                case RED:     return Color.RED;
                case GREEN:   return Color.GREEN;
                case BLUE:    return Color.BLUE;
                case YELLOW:  return Color.YELLOW;
                case VIOLET:  return new Color(0xC000C0);
                case BROWN:   return new Color(0x800000);
            }
        }
        return Color.GRAY;
    }

    public void showAboutDialog() {
        JOptionPane.showMessageDialog(this, "Mastermind\nPiotr Marcińczyk (C) 2015\nLodz University of Technology\nAdvanced Programming Course in Java", "About", JOptionPane.INFORMATION_MESSAGE);
    }

    public GUIExceptionHandler getExceptionHandler() {
        return exceptionHandler;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }
}
