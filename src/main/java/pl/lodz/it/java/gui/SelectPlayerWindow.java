package pl.lodz.it.java.gui;

import pl.lodz.it.java.dao.DAOException;
import pl.lodz.it.java.model.Player;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Window for user selection
 */
public class SelectPlayerWindow extends JFrame {
    private final JList<Player> playersList;
    private final SelectPlayerButtonsPanel buttonsPanel;
    private MainWindow mainWindow;

    public SelectPlayerWindow(MainWindow mainWindow) throws DAOException {
        super("Select the player");
        this.mainWindow = mainWindow;
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        setSize(400, 600);
        setLocationRelativeTo(null);	// default system position
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                if(mainWindow.getLogic().getCurrentPlayer() != null){
                    setVisible(false);
                } else {
                    dispose();
                }
            }
        });

        setLayout(new GridBagLayout());

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.BOTH;
        gbc.weightx = 1.0;
        gbc.weighty = 0.9;
        gbc.gridx = 0;
        gbc.gridy = 0;

        playersList = new JList<Player>();
        reloadPlayers();
        add(playersList, gbc);

        gbc.weighty = 0.1;
        gbc.gridx = 0;
        gbc.gridy = 1;
        buttonsPanel = new SelectPlayerButtonsPanel(this);
        add(buttonsPanel, gbc);

    }

    private void reloadPlayers() throws DAOException {
        Player[] players = mainWindow.getLogic().getDAO().getPlayers();
        DefaultListModel<Player> model = new DefaultListModel<>();
        playersList.setModel(model);
        for(Player p: players) model.addElement(p);
    }

    public void newPlayerButtonAction(ActionEvent event) {
        Object result = JOptionPane.showInputDialog(this, "Name", "New player", JOptionPane.QUESTION_MESSAGE, null, null, "Player");
        if(result == null) return;
        try {
            mainWindow.getLogic().createPlayer(result.toString());
            reloadPlayers();
        } catch (DAOException e) {
            mainWindow.getExceptionHandler().handleException(e);
        }
    }

    public void selectPlayerAction(ActionEvent event) {
        Player player = playersList.getSelectedValue();
        if(player == null) return;
        mainWindow.setPlayer(player);
        mainWindow.setVisible(true);
        setVisible(false);
    }

    public void removePlayerAction(ActionEvent event) {
        Player player = playersList.getSelectedValue();
        int result = JOptionPane.showConfirmDialog(
                mainWindow,
                String.format("Are you sure to remove player %s?", player.getName()),
                "Confirm remove",
                JOptionPane.WARNING_MESSAGE,
                JOptionPane.YES_NO_OPTION
        );
        if(result == JOptionPane.YES_OPTION){
            try {
                mainWindow.getLogic().getDAO().removePlayer(player);
                reloadPlayers();
            } catch (DAOException e) {
                mainWindow.getExceptionHandler().handleException(e);
            }
        }
    }
}
