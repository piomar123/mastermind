package pl.lodz.it.java.gui;

import javax.swing.*;
import java.awt.*;

/**
 * JPanel with buttons for player selection window.
 */
public class SelectPlayerButtonsPanel extends JPanel {
    private final JButton newPlayerButton, selectButton, removePlayerButton;
    private SelectPlayerWindow selectPlayerWindow;

    public SelectPlayerButtonsPanel(SelectPlayerWindow selectPlayerWindow) {
        super(new GridBagLayout());
        this.selectPlayerWindow = selectPlayerWindow;

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.BOTH;
        gbc.weightx = 1./3;
        gbc.weighty = 1;
        gbc.gridx = 0;
        gbc.gridy = 0;

        selectButton = new JButton("Select");
        selectButton.addActionListener(selectPlayerWindow::selectPlayerAction);
        add(selectButton, gbc);

        gbc.gridx = 1;
        newPlayerButton = new JButton("New");
        newPlayerButton.addActionListener(selectPlayerWindow::newPlayerButtonAction);
        add(newPlayerButton, gbc);

        gbc.gridx = 2;
        removePlayerButton = new JButton("Remove");
        removePlayerButton.addActionListener(selectPlayerWindow::removePlayerAction);
        add(removePlayerButton, gbc);
    }
}
