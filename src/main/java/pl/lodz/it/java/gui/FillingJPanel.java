package pl.lodz.it.java.gui;

import javax.swing.*;
import java.awt.*;

/**
 * JPanel with size equal to preferredSize.
 */
public class FillingJPanel extends JPanel {
    @Override
    public Dimension getPreferredSize(){
        return super.getPreferredSize();
    }

    @Override
    public Dimension getMaximumSize() {
        return getPreferredSize();
    }

    @Override
    public Dimension getMinimumSize() {
        return getPreferredSize();
    }
}
