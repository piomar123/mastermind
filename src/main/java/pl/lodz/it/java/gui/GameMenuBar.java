package pl.lodz.it.java.gui;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * Menu bar for the game main window.
 */
public class GameMenuBar extends JMenuBar {
    private final MainWindow mainWindow;
    private JMenu gameMenu, helpMenu;
    private JMenuItem newGameItem, exitGameItem, aboutHelpItem;

    public GameMenuBar(MainWindow mainWindow) {
        this.mainWindow = mainWindow;


        gameMenu = new JMenu("Game");
        gameMenu.setMnemonic('G');

        add(gameMenu);
            newGameItem = new JMenuItem("New");
            newGameItem.setMnemonic('N');
            newGameItem.addActionListener((ActionEvent e) -> mainWindow.startNewGame());
            gameMenu.add(newGameItem);

            exitGameItem = new JMenuItem("End");
            exitGameItem.setMnemonic('E');
            exitGameItem.addActionListener((ActionEvent e) -> mainWindow.pressCloseButton());
            gameMenu.add(exitGameItem);

        helpMenu = new JMenu("Help");
        helpMenu.setMnemonic('H');
        add(helpMenu);
            aboutHelpItem = new JMenuItem("About");
            aboutHelpItem.setMnemonic('A');
            aboutHelpItem.addActionListener((ActionEvent e) -> mainWindow.showAboutDialog());
            helpMenu.add(aboutHelpItem);
    }
}
