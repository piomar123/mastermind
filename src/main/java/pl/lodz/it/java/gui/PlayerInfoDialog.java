package pl.lodz.it.java.gui;

import pl.lodz.it.java.model.Player;

import javax.swing.*;
import java.awt.*;

/**
 * Dialog with player info.
 */
public class PlayerInfoDialog extends JDialog {
    public PlayerInfoDialog(Frame owner, Player player) {
        super(owner, true);
        GroupLayout layout = new GroupLayout(this);
        setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        layout.setVerticalGroup(layout.createSequentialGroup());
    }
}
