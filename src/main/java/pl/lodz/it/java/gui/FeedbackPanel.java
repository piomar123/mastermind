package pl.lodz.it.java.gui;

import pl.lodz.it.java.model.Feedback;

import java.awt.*;

/**
 * Panel for feedback pegs.
 */
public class FeedbackPanel extends FillingJPanel {
    private BoardRowPanel rowPanel;
    private Feedback feedback = null;
    private Color bgColor = new Color(0x808080);

    public FeedbackPanel(BoardRowPanel rowPanel) {
        this.rowPanel = rowPanel;
    }

    @Override
    public Dimension getPreferredSize() {
        int dim = rowPanel.getHeight();
        return new Dimension(dim, dim);
    }


    @Override
    public void paint(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setRenderingHints(rh);
        super.paint(g2);
        g2.setColor(bgColor);
        g2.fillRect(0, 0, getWidth(), getHeight());

        if(feedback == null) return;

        int cx = getWidth()/2, cy = getHeight()/2;

        int black = feedback.getBlack();
        int white = feedback.getWhite();

        int x = (int) (cx * 0.1);
        int y = (int) (cy * 0.1);
        int ncx = cx - 2*x;
        int ncy = cy - 2*y;

        mainLoop:
        for(int i = 0; i < 2; i++){
            for(int j = 0; j < 2; j++){
                Color color = null;
                if(black > 0){
                    color = Color.BLACK;
                    black--;
                } else if(white > 0){
                    color = Color.WHITE;
                    white--;
                }
                if(color == null) break mainLoop;

                g2.setColor(color);
                g2.fillOval(x + j * cx, y + i * cy, ncx, ncy);
            }
        }
    }

    public Feedback getFeedback() {
        return feedback;
    }

    public void setFeedback(Feedback feedback) {
        this.feedback = feedback;
        invalidate();
    }
}
