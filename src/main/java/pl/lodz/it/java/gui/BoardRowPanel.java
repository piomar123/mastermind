package pl.lodz.it.java.gui;

import pl.lodz.it.java.model.CodePeg;
import pl.lodz.it.java.model.Feedback;
import pl.lodz.it.java.model.RowState;

import javax.swing.*;
import java.awt.*;

/**
 * Panel for single row with pegs.
 */
public class BoardRowPanel extends FillingJPanel {
    private final RowNumberPanel numberPanel;
    private GameboardScrollPane gameboardScrollPane;
    private boolean interactive;
    private MainWindow mainWindow;
    private CodePegPanel[] pegPanels;
    private FeedbackPanel feedbackPanel;
    private boolean correct = false;

    /**
     * Creates row panel with given pegs state and feedback.
     * @param parent reference to gameboard
     * @param iteration number of iteration to show
     * @param rowState state of pegs to display
     * @param feedback feedback to given row state
     * @param interactive if true, allows player to change pegs colors
     */
    public BoardRowPanel(GameboardScrollPane parent, int iteration, RowState rowState, Feedback feedback, boolean interactive) {
        gameboardScrollPane = parent;
        this.interactive = interactive;
        this.mainWindow = gameboardScrollPane.getMainWindow();
        this.numberPanel = new RowNumberPanel(this, iteration);
        setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        add(numberPanel);
        pegPanels = new CodePegPanel[RowState.PEGS_PER_ROW];

        for(int i = 0; i < RowState.PEGS_PER_ROW; i++){
            pegPanels[i] = new CodePegPanel(this, interactive);
            add(pegPanels[i]);
        }

        if(rowState != null){
            CodePeg[] pegs = rowState.getPegs();
            for(int i = 0; i < pegPanels.length; i++){
                pegPanels[i].setPeg(pegs[i]);
            }
        }
        add(Box.createHorizontalGlue());

        feedbackPanel = new FeedbackPanel(this);
        feedbackPanel.setFeedback(feedback);
        add(feedbackPanel);

    }

    /**
     * Creates interactive empty row panel with greyed pegs & no feedback.
     * @param parent reference to gameboard
     * @param iteration number of iteration to show
     */
    public BoardRowPanel(GameboardScrollPane parent, int iteration) {
        this(parent, iteration, null, null, true);
    }

    public GameboardScrollPane getGameboardScrollPane() {
        return gameboardScrollPane;
    }

    @Override
    public Dimension getPreferredSize() {
        int width = gameboardScrollPane.getPanel().getWidth();
        return new Dimension(width, (int) (width/6. +.5));
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        int cx = getWidth() - 1, cy = getHeight() - 1;
//        g.setColor(Color.GRAY);
//        g.fillRect(0, 0, cx, cy);
        g.setColor(Color.BLACK);
        g.drawRect(0, 0, cx, cy );
    }

    /**
     * Returns selected pegs state by player.
     * @return RowState with pegs or null if not all pegs chosen
     */
    public RowState getRow() {
        CodePeg[] pegs = new CodePeg[pegPanels.length];
        for (int i = 0; i < pegs.length; i++) {
            pegs[i] = pegPanels[i].getPeg();
            if(pegs[i] == null) return null;
        }
        return new RowState(pegs);
    }

    public void setFeedback(Feedback feedback) {
        feedbackPanel.setFeedback(feedback);
    }

    public void setInteractive(boolean interactive) {
        if(this.interactive == interactive) return;
        this.interactive = interactive;
        for(int i = 0; i < pegPanels.length; i++){
            pegPanels[i].setInteractive(interactive);
        }
    }

    public void setCorrect() {
        correct = true;
        numberPanel.setCorrect();
    }
}
