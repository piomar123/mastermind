package pl.lodz.it.java.gui;

import javax.swing.*;

/**
 * Exception handler for GUI.
 */
public class GUIExceptionHandler {
    private MainWindow parent;

    public GUIExceptionHandler(MainWindow parent) {
        this.parent = parent;
    }

    public void handleException(Exception e) {
        handleStatic(e, parent);
    }

    public static void handleStatic(Exception e) {
        handleStatic(e, null);
    }

    public static void handleStatic(Exception e, JFrame parent) {
        JOptionPane.showMessageDialog(parent, e.toString(), "Error", JOptionPane.ERROR_MESSAGE);
    }
}
