package pl.lodz.it.java.gui;

import pl.lodz.it.java.model.CodePeg;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Arrays;

/**
 * Panel for code peg in a row.
 */
public class CodePegPanel extends FillingJPanel implements MouseListener {
    private BoardRowPanel rowPanel;
    private CodePeg peg = null;
    private boolean interactive;
    private boolean mouseInside;
    private Stroke outlineStroke = new BasicStroke(3);

    public CodePegPanel(BoardRowPanel rowPanel, boolean interactive) {
        this.rowPanel = rowPanel;
        setInteractive(interactive);
    }

    @Override
    public Dimension getPreferredSize() {
        int dim = rowPanel.getHeight();
        return new Dimension(dim, dim);
    }

    @Override
    public void paint(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setRenderingHints(rh);
        super.paint(g2);
        int cx = getWidth(), cy = getHeight();
        int x = (int) (cx * 0.1);
        int y = (int) (cy * 0.1);
        cx -= 2*x;
        cy -= 2*y;

        Color fillColor = rowPanel.getGameboardScrollPane().getMainWindow().pegToColor(peg);
        g2.setColor(fillColor);
        g2.fillOval(x, y, cx, cy);

        if(mouseInside){
            Stroke defaultStroke = g2.getStroke();
            g2.setColor(Color.BLACK);
            g2.setStroke(outlineStroke);
            g2.drawOval(x, y, cx, cy);
            g2.setStroke(defaultStroke);
        }

        g2.setColor(Color.BLACK);
        g2.drawRect(0, 0, getWidth(), getHeight());
    }

    public CodePeg getPeg() {
        return peg;
    }

    public void setPeg(CodePeg peg) {
        this.peg = peg;
        callRepaint();
    }

    public boolean isInteractive() {
        return interactive;
    }

    public void setInteractive(boolean interactive) {
        this.interactive = interactive;
        if(interactive) {
            addMouseListener(this);
        } else {
            removeMouseListener(this);
            mouseInside = false;
        }
    }

    private void setMouseInside(boolean inside) {
        mouseInside = inside;
        callRepaint();
    }

    public boolean isMouseInside() {
        return mouseInside;
    }

    @Override public void mousePressed(MouseEvent e) {
        CodePeg[] colors = CodePeg.values();
        if(peg == null) {
            setPeg(colors[0]);
        } else {
            int button = e.getButton();
            int i = peg.getValue();
            if(button == MouseEvent.BUTTON1){
                i += 1;
                i %= colors.length;
            } else if(button == MouseEvent.BUTTON3) {
                i = (i > 0) ? (i - 1) : (colors.length - 1);
            } else return;
            setPeg(colors[i]);
        }
    }

    private void callRepaint() {
        rowPanel.repaint();
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        setMouseInside(true);
    }

    @Override
    public void mouseExited(MouseEvent e) {
        setMouseInside(false);
    }

    @Override public void mouseReleased(MouseEvent e) { }
    @Override public void mouseClicked (MouseEvent e) { }

}
