package pl.lodz.it.java.model;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class RowStateTest {

    @Test
    public void shouldCalculatePegsFromSum() {
        //given
        CodePeg[] pegs = new CodePeg[] {CodePeg.RED, CodePeg.GREEN, CodePeg.BLUE, CodePeg.YELLOW};
        //when
        RowState sut = new RowState(pegs);
        //then
        assertEquals(sut.getState(), 51);
    }

    @Test
    public void shouldCalculateSumFromPegs() {
        //given
        RowState sut = new RowState(51);
        //when
        CodePeg[] pegs = new CodePeg[] {CodePeg.RED, CodePeg.GREEN, CodePeg.BLUE, CodePeg.YELLOW};
        //then
        assertArrayEquals(sut.getPegs(), pegs);
    }
}