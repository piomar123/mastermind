package pl.lodz.it.java.logic;

import org.junit.Before;
import org.junit.Test;
import pl.lodz.it.java.model.CodePeg;
import pl.lodz.it.java.model.RowState;

import static org.junit.Assert.*;

public class HelpLogicTest {
    private HelpLogic sut;

    @Before
    public void setUp() throws Exception {
        sut = new HelpLogic();
    }

    @Test
    public void shouldGetProperNumberOfPermutations() {
        //given
        //when
        assertTrue(RowState.PEGS_PER_ROW == 4 && CodePeg.COLORS == 6);
        //then
        assertEquals(sut.getNumberOfPermutations(), 1296);
    }

    @Test
    public void shouldCalculatePowers() {
        assertEquals(1, sut.power(9, 0));
        assertEquals(2, sut.power(2, 1));
        assertEquals(9, sut.power(3, 2));
        assertEquals(25, sut.power(-5, 2));
        assertEquals(-27, sut.power(-3, 3));
        assertEquals(1000, sut.power(10, 3));
    }
}