package pl.lodz.it.java.logic;

import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import junitparams.JUnitParamsRunner;
import pl.lodz.it.java.model.CodePeg;
import pl.lodz.it.java.model.Feedback;
import pl.lodz.it.java.model.RowState;

import static junitparams.JUnitParamsRunner.$;
import static org.junit.Assert.assertEquals;


@RunWith(JUnitParamsRunner.class)
public class MastermindFeedbackParamsTest {

    private HelpLogic sut;

    @Before
    public void setUp() throws Exception {
        sut = new HelpLogic();
    }

    @Test
    @Parameters
    public void testProvideFeedback(RowState guess, RowState solved, int black, int white) {
        Feedback feedback = sut.provideFeedback(guess, solved);
        assertEquals(feedback.getBlack(), black);
        assertEquals(feedback.getWhite(), white);
    }

    public Object[] parametersForTestProvideFeedback() {
        RowState solved1 = new RowState(CodePeg.RED, CodePeg.GREEN, CodePeg.BLUE, CodePeg.YELLOW);
        RowState solved2 = new RowState(CodePeg.GREEN, CodePeg.GREEN, CodePeg.GREEN, CodePeg.GREEN);
        return $(
                $(solved1, solved1, 4, 0),
                $(solved2, solved2, 4, 0),

                $(new RowState(CodePeg.GREEN,   CodePeg.BLUE,  CodePeg.YELLOW,  CodePeg.RED), solved1, 0, 4),
                $(new RowState(CodePeg.YELLOW,  CodePeg.GREEN, CodePeg.BROWN,   CodePeg.RED), solved1, 1, 2),
                $(new RowState(CodePeg.YELLOW,  CodePeg.GREEN, CodePeg.RED,     CodePeg.RED), solved1, 1, 2),
                $(new RowState(CodePeg.RED,     CodePeg.GREEN, CodePeg.RED,     CodePeg.RED), solved1, 2, 0),
                $(new RowState(CodePeg.BROWN,   CodePeg.VIOLET,CodePeg.RED,     CodePeg.RED), solved1, 0, 1),

                $(new RowState(CodePeg.GREEN,   CodePeg.BLUE,  CodePeg.YELLOW,  CodePeg.RED), solved2, 1, 0),
                $(new RowState(CodePeg.YELLOW,  CodePeg.GREEN, CodePeg.BROWN,   CodePeg.RED), solved2, 1, 0),
                $(new RowState(CodePeg.YELLOW,  CodePeg.GREEN, CodePeg.RED,     CodePeg.RED), solved2, 1, 0),
                $(new RowState(CodePeg.RED,     CodePeg.GREEN, CodePeg.RED,     CodePeg.RED), solved2, 1, 0),
                $(new RowState(CodePeg.BROWN,   CodePeg.VIOLET,CodePeg.RED,     CodePeg.RED), solved2, 0, 0)
        );
    }

}